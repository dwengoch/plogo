
function msg = encode (cmd, lang="en")

    table_l = {'forward','backward','leftturn','rightturn'};
    table_s = {'fw','bw','lt','rt'};
    
    [f, p] = sscanf (cmd, '%s%d','C');

    [tf,x] = ismember (f,table_l);
    if !tf
        [tf,x] = ismember (f,table_s);
        if !tf
            error('unknown command');
        end
    end
    
    msg = [x p];

end
