#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>
#include <DwenguinoMotor.h>
#include <DwenguinoSensorPanel.h>

SensorPanel panel;

DCMotor dcMotor1(MOTOR_1_0, MOTOR_1_1);
DCMotor dcMotor2(MOTOR_2_0, MOTOR_2_1);
unsigned int M_Speed = 150;

typedef void (* CommandPtr) (const int);

#define N_RESRV 7
CommandPtr cmd[N_RESRV] = {forward, backward, leftturn, rightturn, move, rotate, lights};

void forward (const int d)
{
  move (d);
};
void backward (const int d)
{
  move (-d);
};
void leftturn (const int d)
{
  rotate (d);
};
void rightturn (const int d)
{
  rotate (-d);
};

void move (const int d)
{
    char dir = (d < 0)? -1 : 1;
    unsigned int D = abs (d);      
    
    dcMotor1.setSpeed (dir*M_Speed);
    dcMotor2.setSpeed (dir*M_Speed);
    delay (D);
    stop ();
};

void rotate (const int d)
{
    char dir = (d < 0)? -1 : 1;
    unsigned int D = abs (d);      

    dcMotor1.setSpeed(-dir*M_Speed);
    dcMotor2.setSpeed(dir*M_Speed);
    delay (D);
    stop ();
};

void stop ()
{
    dcMotor1.setSpeed(0);
    dcMotor2.setSpeed(0);
};

void lights (const int d)
{
  unsigned char S[2] = {0, 0};
  switch (d)
  {
    case 0:
      S[0] = 0; S[1] = 0;
      break;
    case 1:
      S[0] = 0; S[1] = 1;
      break;
    case 2:
      S[0] = 1; S[1] = 0;
      break;
    case 3:
      S[0] = 1; S[1] = 1;
      break;
  }
  panel.setHeadlights(LD1,S[1]);
  panel.setHeadlights(LD2,S[0]);
};

void setup() {
    initDwenguino();
    
    panel = SensorPanel();
    panel.init();

    Serial1.begin(9600);    // Serial1 oepn communication with the Bluetooth module
                            // at 9600 baud rate.
    // Write the messages to show to the user.
    dwenguinoLCD.setCursor(0,0);
    dwenguinoLCD.print("Input: ");

}

void loop() {
    // Listen for data on the serial connection
    if ( Serial1.available() > 0 )
    {
       // TODO: LEX and PARSE
       int cmd_idx = Serial1.parseInt();
       int cmd_prm = Serial1.parseInt();
       
       // Print them to the screen after "Input"
       dwenguinoLCD.setCursor(6,0);
       dwenguinoLCD.print("         ");
       dwenguinoLCD.setCursor(6,0);
       dwenguinoLCD.print(cmd_idx);
       dwenguinoLCD.print(" ");
       dwenguinoLCD.print(cmd_prm);
       
       cmd[cmd_idx](cmd_prm);
       
       Serial1.println(0);
       
    }

}
